import api from '../../api/api'
var app = getApp()
import {
  myinvitecode
} from '../../api/conf'
import {
  weixinopenphonenumber
} from '../../api/conf'
import {
  banner
} from '../../api/conf'
import {
  agentsupgrade
} from '../../api/conf'
import {
  agentspostinfo
} from '../../api/conf'
Page({
  data: {
    money: '-',
    invite: '',
    islogin: false,
    phone: '',
    myBanner: '',
    userName:'',
    hiddenmodalput:true,
  },
  onLoad: function() {
    this.getMybanner()
  },
  onShow: function() {
    if (wx.getStorageSync('phone')){
      this.setData({
        phone: wx.getStorageSync('phone')
      })
    }
  },
  clickRecharge: function() {
    wx.navigateTo({
      url: '/pages/recharge/recharge'
    })
  },
  contactCallback: function (e) {
    var path = e.detail.path, query = e.detail.query, params = '';
    if (path) {
      for (var key in query) {
        params = key + '=' + query[key] + '&';
      }
      params = params.slice(0, params.length - 1);
      wx.navigateTo({
        url: path + '?' + params
      })
    }
  },
  sendInvite: function() {
    var that = this
    wx.showLoading({
      title: '加载中',
    })
    api.get(myinvitecode, {

    }).then(res => {
      wx.hideLoading()
      that.setData({
        invite: res.Data
      })
      wx.showModal({
        title: '邀请码',
        content: res.Data.Code,
        confirmText: '复制',
        success(res) {
          if (res.confirm) {
            wx.setClipboardData({
              data: that.data.invite.Code,
              success(res) {
                wx.showToast({
                  title: '复制成功',
                  icon: 'none',
                })
              }
            })
          } else if (res.cancel) {

          }
        }
      })
    })
  },
  out: function() {
    wx.showModal({
      title: '确定退出？',
      content: '点击确定将退出小程序',
      success: function(res) {
        if (res.confirm) {
          wx.clearStorage()
          wx.navigateTo({
            url: '/pages/login/login',
          })
        } else {

        }
      }
    })
  },
  getPhoneNumber(e) {
    var that = this
    wx.login({
      success(res) {
        if (res.code) {
          api.get(weixinopenphonenumber, {
            code: res.code,
            encryptedData: e.detail.encryptedData,
            iv: e.detail.iv
          }).then(res => {
            if (res.Code == 200) {
              wx.setStorageSync('phone', res.Data.phone)
              that.setData({
                islogin: false,
                phone: wx.getStorageSync('phone')
              })
              wx.showToast({
                title: '获取手机号成功',
              })
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      },
      fail(err) {
        wx.showToast({
          title: '登录超时',
          icon: 'none'
        })
      }
    })
  },
  getMybanner:function(){
    var that = this
    api.get(banner,{
      name: 'mybanner'
    }).then(res=>{
      that.setData({
        myBanner: res.Data.Items
      })
    })
  },
  clickParner: function() {
    var that = this
    api.get(agentsupgrade).then(res => {
      if(res.Code==220){
        this.setData({
          hiddenmodalput: false
        })
      }
       else if (res.Code == 200) {
         wx.navigateTo({
           url: '/pages/distribution/distribution'
         })
       }
      else{
        this.setData({
          hiddenmodalput: false
        })
      }
    }).catch(err => {
      if(err.Code==220){
        this.setData({
          hiddenmodalput: false
        })
      }else{
        wx.showModal({
          title: '提示',
          content: '审核中'
        })
      }
    })
  },
  changeUserName: function(e) {
    this.setData({
      userName: e.detail.value
    })
  },
  changePhone: function(e) {
    this.setData({
      phone: e.detail.value
    })
  },
  confirm: function() {
    var that = this
    // var data = e.detail.value
    wx.showLoading({
      title: '正在提交',
    })
    if (this.data.userName == '') {
      wx.showToast({
        title: '姓名不能为空',
        icon: 'none'
      })
    } else if (this.data.phone == '') {
      wx.showToast({
        title: '手机号不能为空',
        icon: 'none'
      })
    }else{ 
      api.post(agentspostinfo, {
        RealName: this.data.userName,
        Phone: this.data.phone
      }).then(data => {
        if (data.Code == 200) {
          wx.showToast({
            title: '提交成功',
            icon: '',
          })
          this.setData({
            hiddenmodalput: true,
          })
          wx.navigateTo({
            url: '/pages/distribution/distribution'
          })
        }
        wx.hideLoading()
      }).catch(res => {
        wx.showToast({
          title: res.Content,
          icon: '',
        })
        wx.hideLoading()
      })
    }
  },
  cancel: function(e) {
    this.setData({
      hiddenmodalput: !this.data.hiddenmodalput,
    })
  },
})