import api from '../../api/api'
import {
  category
} from '../../api/conf'
import {
  searCate
} from '../../api/conf'
import {
  search
} from '../../api/conf'
Page({
  data: {
    curIndex: 0,
    list: [],
    cateId: null,
    produList: [],
    page: 1,
    reTurn: false,
    searchPage: false,
    searchContent: [],
    searpage: 1,
    Qvaule: null,
    frist: false,
    searclosebtn: false,
    num: 0,
    orderBy: 0,
    cateName: '',
    subCate: []
  },
  onLoad: function(options) {

  },
  onReady: function() {
    var that = this
    api.get(category, {}).then(res => {
      that.setData({
        cateId: res.Data[0].Id
      })
      let cateListArr = res.Data
      let all = {
        Id: 0,
        Name: '全部',
        SeName: 'cat-0'
      }
      // cateListArr.push(all)
      that.setData({
        list: cateListArr,
        frist: true
      })
      this.getProduct()
    }).catch(err => {
      wx.showToast({
        title: err.message,
        icon: 'none'
      })
    })
  },
  onShow: function() {
    if (this.data.frist) {
      this.setData({
        produList: [],
        page: 1,
        reTurn: false
      })
      this.getProduct()
      this.getSubCate()
    }
  },
  getSubCate: function() {
    
    var that = this
    api.get(category, {
      catId: that.data.cateId
    }).then(res => {
      that.setData({
        subCate: res.Data
      })
    })
  },
  categohref: function() {
    if (this.data.produList.length) {
      wx.navigateTo({
        url: '/pages/subcategory/subcategory?id=' + this.data.cateId + '&subid=' + this.data.produList[0].Id,
      })
    }
  },
  switchRightTab: function(e) {
    let index = parseInt(e.target.dataset.index);
    let catId = e.target.dataset.cateid
    let cateName = e.target.dataset.name
    this.setData({
      curIndex: index,
      cateId: catId,
      produList: [],
      subCate: [],
      page: 1,
      reTurn: false,
      num: 0,
      orderBy: 0,
      cateName: cateName
    })
    this.getProduct()
    this.getSubCate()
  },
  search: function(e) {
    var that = this
    this.setData({
      Qvaule: e.detail.value,
      searpage: 1,
      searclosebtn: true
    })
    wx.showNavigationBarLoading()
    api.get(search, {
      q: that.data.Qvaule,
      page: that.data.searpage
    }).then(res => {
      that.setData({
        searchContent: res.Data.Items,
        searchPage: true
      })
      wx.hideNavigationBarLoading()
    })
  },
  getProduct: function() {
    var that = this
    if (this.data.reTurn) {
      return
    }
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
    api.get(searCate, {
      cid: that.data.cateId,
      page: that.data.page
    }).then(res => {
      var showMore = (that.data.produList.concat(res.Data.Items).length + that.data.subCate.length) >= 12;
      that.setData({
        produList: that.data.produList.concat(res.Data.Items),
        showMore: showMore?true:false
      })
      wx.hideLoading()
    })
  },
  lower: function(e) {
    this.setData({
      page: this.data.page + 1
    })
    this.getProduct()
  },
  //关闭搜索页面
  closePage: function() {
    this.setData({
      Qvaule:'',
      searchPage: false,
      searclosebtn: false
    })
  },
  //搜索页面滚动加载
  searchLower: function() {
    var that = this
    this.setData({
      searpage: this.data.searpage + 1
    })
    wx.showNavigationBarLoading()
    api.get(search, {
      q: that.data.Qvaule,
      page: that.data.searpage
    }).then(res => {
      that.setData({
        searchContent: that.data.searchContent.concat(res.Data.Items)
      })
      wx.hideNavigationBarLoading()
    })
  },
  menuClick: function(e) {
    let mum = e.currentTarget.dataset.num
    let by = this.data.orderBy
    if (mum == 1 && by != 21) {
      by = 21
    } else if (mum == 1 && by == 21) {
      by = 20
    } else if (mum == 3 && by != 6) {
      by = 6
    } else if (mum == 3 && by == 6) {
      by = 5
    } else if (mum == 2 && by != 25) {
      by = 25
    } else if (mum == 2 && by == 25) {
      by = 26
    } else {
      by = 0
    }
    this.setData({
      produList: [],
      num: mum,
      orderBy: by,
      page: 1,
      reTurn: false
    })
    this.getProduct()
  }
})