﻿using FluentValidation;
using Urs.Admin.Models.Stores;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Stores
{
    public class GoodsSpecValueValidator : BaseUrsValidator<GoodsSpecValueModel>
    {
        public GoodsSpecValueValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Store.Goods.GoodsVariantAttributes.Attributes.Values.Fields.Name.Required"));
        }
    }
}