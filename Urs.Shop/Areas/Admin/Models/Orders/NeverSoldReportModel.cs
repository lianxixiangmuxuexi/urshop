﻿using System;
using System.ComponentModel.DataAnnotations;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Orders
{
    public partial class NeverSoldReportModel : BaseModel
    {
        [UrsDisplayName("Admin.SalesReport.NeverSold.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [UrsDisplayName("Admin.SalesReport.NeverSold.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }
    }
}