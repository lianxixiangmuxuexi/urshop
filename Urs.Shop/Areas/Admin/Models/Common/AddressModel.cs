﻿using FluentValidation.Attributes;
using Urs.Admin.Validators.Common;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Common
{
    [Validator(typeof(AddressValidator))]
    public partial class AddressModel : BaseEntityModel
    {

        [UrsDisplayName("Admin.Address.Fields.FirstName")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Address.Fields.Company")]
        
        public string Company { get; set; }

        [UrsDisplayName("Admin.Address.Fields.ProvinceId")]
        
        public string ProvinceName { get; set; }

        [UrsDisplayName("Admin.Address.Fields.CityId")]
        
        public string CityName { get; set; }

        [UrsDisplayName("Admin.Address.Fields.AreaId")]
        
        public string AreaName { get; set; }

        [UrsDisplayName("Admin.Address.Fields.Address1")]
        
        public string Address1 { get; set; }

        [UrsDisplayName("Admin.Address.Fields.Address2")]
        
        public string Address2 { get; set; }
        [UrsDisplayName("Admin.Address.Fields.Latitude")]
        public decimal Latitude { get; set; }
        [UrsDisplayName("Admin.Address.Fields.Longitude")]
        public decimal Longitude { get; set; }

        [UrsDisplayName("Admin.Address.Fields.ZipPostalCode")]
        
        public string ZipPostalCode { get; set; }

        [UrsDisplayName("Admin.Address.Fields.PhoneNumber")]
        
        public string PhoneNumber { get; set; }

        [UrsDisplayName("Admin.Address.Fields.FaxNumber")]
        
        public string FaxNumber { get; set; }

        [UrsDisplayName("Admin.Address")]
        public string AddressHtml { get; set; }
        public bool NameEnabled { get; set; }
        public bool NameRequired { get; set; }
        public bool CompanyEnabled { get; set; }
        public bool CompanyRequired { get; set; }
        public bool ProvincesEnabled { get; set; }
        public bool LatitudeEnabled { get; set; }
        public bool LongitudeEnabled { get; set; }
        public bool StreetAddressEnabled { get; set; }
        public bool StreetAddressRequired { get; set; }
        public bool StreetAddress2Enabled { get; set; }
        public bool StreetAddress2Required { get; set; }
        public bool ZipPostalCodeEnabled { get; set; }
        public bool ZipPostalCodeRequired { get; set; }
        public bool PhoneEnabled { get; set; }
        public bool PhoneRequired { get; set; }
        public bool FaxEnabled { get; set; }
        public bool FaxRequired { get; set; }
    }
}