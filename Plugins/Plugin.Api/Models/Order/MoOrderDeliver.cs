﻿namespace Plugin.Api.Models.Order
{
    /// <summary>
    /// 订单确认收货
    /// </summary>
    public partial class MoOrderDeliver
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string Code { get; set; }
    }
}