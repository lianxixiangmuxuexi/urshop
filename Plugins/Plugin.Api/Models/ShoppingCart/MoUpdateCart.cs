﻿namespace Plugin.Api.Models.ShoppingCart
{
    /// <summary>
    /// 修改购物车子项
    /// </summary>
    public class MoUpdateCart 
    {
        /// <summary>
        /// 购物车子项Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 子项数量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 是否选中
        /// </summary>
        public bool Selected { get; set; }
    }
}