﻿using Plugin.Api.Models.Media;
using System.Collections.Generic;

namespace Plugin.Api.Models.ShoppingCart
{
    /// <summary>
    /// 购物车
    /// </summary>
    public partial class MoCart 
    {
        public MoCart()
        {
            Items = new List<MoCartItem>();
            Warnings = new List<string>();
        }
        /// <summary>
        /// 购物车子项
        /// </summary>
        public IList<MoCartItem> Items { get; set; }
        /// <summary>
        /// 警告
        /// </summary>
        public IList<string> Warnings { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        public string OrderTotal { get; set; }

        #region Nested Classes
        /// <summary>
        /// 购物车子项
        /// </summary>
        public partial class MoCartItem
        {
            public MoCartItem()
            {
                Picture = new MoPicture();
                Warnings = new List<string>();
            }
            /// <summary>
            /// 购物车子项Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 商品sku
            /// </summary>
            public string Sku { get; set; }
            /// <summary>
            /// 图片
            /// </summary>
            public MoPicture Picture { get; set; }
            /// <summary>
            /// 商品Id
            /// </summary>
            public int GoodsId { get; set; }
            /// <summary>
            /// 是否选中
            /// </summary>
            public bool Selected { get; set; }
            /// <summary>
            /// 商品名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 商品url
            /// </summary>
            public string SeName { get; set; }
            /// <summary>
            /// 市场价
            /// </summary>
            public string MarketPrice { get; set; }
            /// <summary>
            /// 单价
            /// </summary>
            public string UnitPrice { get; set; }
            /// <summary>
            /// 小计
            /// </summary>
            public string SubTotal { get; set; }
            /// <summary>
            /// 数量
            /// </summary>
            public int Qty { get; set; }
            /// <summary>
            /// 属性
            /// </summary>
            public string AttributeInfo { get; set; }
            /// <summary>
            /// 警告
            /// </summary>
            public IList<string> Warnings { get; set; }

        }

        #endregion
    }
}