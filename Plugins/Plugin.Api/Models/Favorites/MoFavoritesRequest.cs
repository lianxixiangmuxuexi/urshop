﻿namespace Plugin.Api.Models.Favorites
{
    /// <summary>
    /// 添加收藏夹
    /// </summary>
    public partial class MoFavoritesRequest
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 商品Id
        /// </summary>
        public int GoodsId { get; set; }
    }
}