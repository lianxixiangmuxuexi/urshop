﻿namespace Plugin.Api.Models.Goods
{
    /// <summary>
    /// 商品参数
    /// </summary>
    public class MoGoodsSpec
    {
        /// <summary>
        /// 参数Id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 参数名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 参数值Id
        /// </summary>
        public int valid { get; set; }
        /// <summary>
        /// 参数值名称
        /// </summary>
        public string valname { get; set; }
    }
}
