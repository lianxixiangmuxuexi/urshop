﻿namespace Plugin.Api.Models.GoodsReview
{
    /// <summary>
    /// 商品评论头部（好评、中评、差评）
    /// </summary>
    public partial class MoReviewsInfo
    {
        public MoReviewsInfo()
        {
            GeneralInfo = new RatingInfo();
            PositiveInfo = new RatingInfo();
            NeutralInfo = new RatingInfo();
            NegativeInfo = new RatingInfo();
        }
        /// <summary>
        /// 商品Id
        /// </summary>
        public int GoodsId { get; set; }
        /// <summary>
        /// 全部概括
        /// </summary>
        public RatingInfo GeneralInfo { get; set; }
        /// <summary>
        /// 好评概括
        /// </summary>
        public RatingInfo PositiveInfo { get; set; }
        /// <summary>
        /// 中评概括
        /// </summary>
        public RatingInfo NeutralInfo { get; set; }
        /// <summary>
        /// 差评概括
        /// </summary>
        public RatingInfo NegativeInfo { get; set; }

        public partial class RatingInfo
        {
            /// <summary>
            /// 评分平均分（比如4.5）
            /// </summary>
            public decimal Rating { get; set; }
            /// <summary>
            /// 评分总分
            /// </summary>
            public int RatingSum { get; set; }
            /// <summary>
            /// 评分百分比（比如百分之九十返回值为 90）
            /// </summary>
            public int RatingPercent { get; set; }
            /// <summary>
            /// 评价数量
            /// </summary>
            public int ReviewTotal { get; set; }
        }
    }

}