﻿namespace Plugin.Api.Models.Common
{
    public partial class MoCoupon
    {
        /// <summary>
        /// 优惠券Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 面值
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndTime { get; set; }
        /// <summary>
        /// 最低消费
        /// </summary>
        public string MinimumConsumption { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string PictureUrl { get; set; }
    }
}