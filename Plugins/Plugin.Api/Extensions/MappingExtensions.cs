﻿using System;
using System.Linq;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Common;
using Urs.Data.Domain.Users;
using Urs.Core.Infrastructure;
using Urs.Data.Domain.Configuration;
using Plugin.Api.Models.Catalog;
using Plugin.Api.Models.Common;
using Plugin.Api.Models.UserAddress;
using Urs.Services.Common;
using Urs.Services.Users;

namespace Plugin.Api.Extensions
{
    public static class MappingExtensions
    {
        //category
        public static MoCategory ToModel(this Category entity)
        {
            if (entity == null)
                return null;

            var model = new MoCategory()
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
            };
            return model;
        }
        public static MoBrand ToModel(this Brand entity)
        {
            if (entity == null)
                return null;

            var model = new MoBrand()
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
            };
            return model;
        }

        public static MoUserSearch ToModel(this User entity)
        {
            if (entity == null)
                return null;

            var model = new MoUserSearch()
            {
                Id = entity.Id,
                Name = entity.Nickname,
            };
            return model;
        }
        public static void PrepareModel(this MoAddress model,
            Address address, bool excludeProperties,
            AddressSettings addressSettings)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (addressSettings == null)
                throw new ArgumentNullException("addressSettings");

            if (!excludeProperties && address != null)
            {
                model.Id = address.Id;
                model.Name = address.Name;
                model.ProvinceName = address.ProvinceName;
                model.CityName = address.CityName;
                model.AreaName = address.AreaName;
                model.Address = address.Address1;
                model.Phone = address.PhoneNumber;
            }
        }
        public static Address ToEntity(this MoAddressEdit model)
        {
            if (model == null)
                return null;
            var entity = new Address();
            return ToEntity(model, entity);
        }
        public static Address ToEntity(this MoAddressEdit model, Address destination)
        {
            if (model == null)
                return destination;
            destination.Name = model.Name;
            destination.ProvinceName = model.ProvinceName;
            destination.CityName = model.CityName;
            destination.AreaName = model.AreaName;
            destination.Address1 = model.Address;
            destination.PhoneNumber = model.Phone;
            return destination;
        }

    }
}