﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;

namespace Plugin.Api.SwaggerGen
{
    /// <summary>
    /// Swagger注释帮助类
    /// </summary>
    public partial class SwaggerDocTag : IDocumentFilter
    {
        /// <summary>
        /// 添加附加注释
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="context"></param>
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Info.Title = "优社电商 V1 API文档";
            swaggerDoc.Info.Description = "authorization:登陆后的用户标识AccessToken,<br/>" +
                "guid:初始化时的用户标识Guid,如果用户登陆可能导致Guid不一致，需更新Guid<br/>" +
                "支付宝手机版接口Api:plugins/paymentalipaywap/payingapi?orderid=xxx&paytype=Order||Recharge<br/>" +
                "微信支付手机版接口Api:plugins/paymentweixinwap/payingapi?orderid=xxx&paytype=Order||Recharge";

            if (swaggerDoc.Tags == null) swaggerDoc.Tags = new List<Tag>();
            var list = new List<Tag>
            {
                //添加对应的控制器描述 这个是我好不容易在issues里面翻到的
                new Tag { Name = "Account", Description = "账号" },
                new Tag { Name = "Address", Description = "地址列表[Auth]" },
                new Tag { Name = "Category", Description = "商品分类" },
                new Tag { Name = "Checkout", Description = "结算下单[Auth]" },
                new Tag { Name = "Common", Description = "辅助工具" },
                new Tag { Name = "User", Description = "用户信息[Auth]" },
                new Tag { Name = "Brand", Description = "品牌制造商" },
                new Tag { Name = "Order", Description = "订单信息[Auth]" },
                new Tag { Name = "Goods", Description = "商品信息" },
                new Tag { Name = "Review", Description = "评论信息" },
                new Tag { Name = "Search", Description = "搜索" },
                new Tag { Name = "ShoppingCart", Description = "购物车" },
                new Tag { Name = "Topic", Description = "单页信息" },
                new Tag { Name = "Coupon", Description = "优惠券[Auth]" },
                new Tag { Name = "Shipping", Description = "配送信息" },
                new Tag { Name = "Shop", Description = "店铺信息" }
            };
            foreach (var item in list)
                swaggerDoc.Tags.Add(item);
        }
    }
}
