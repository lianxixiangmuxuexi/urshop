﻿using Microsoft.AspNetCore.Mvc;
using Urs.Core;
using Urs.Services.Common;
using Urs.Services.Localization;

namespace Urs.Plugin.Payments.WeixinOpen.Components
{
    [ViewComponent(Name = WeixinOpenPaymentDefaults.ViewComponentName)]
    public class PaymentWeixinOpenViewComponent : ViewComponent
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public PaymentWeixinOpenViewComponent(ILocalizationService localizationService,
            IWorkContext workContext)
        {
            this._localizationService = localizationService;
            this._workContext = workContext;
        }

        #endregion

        #region Methods

        public IViewComponentResult Invoke()
        {

            return View("~/Plugins/Payments.WeixinOpen/PaymentInfo.cshtml");
        }

        #endregion
    }
}