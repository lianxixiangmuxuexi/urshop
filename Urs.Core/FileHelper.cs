﻿using System;
using System.IO;

namespace Urs.Core
{
    public class FileHelper
    {
        #region IsExistFolder 检测指定目录是否存在
        /// <summary>
        /// 检测指定目录是否存在
        /// </summary>
        /// <param name="path">目录的绝对路径</param>
        /// <returns></returns>
        public static bool IsExistFolder(string path)
        {
            return Directory.Exists(path);
        }
        #endregion

        #region IsExistFile 检测指定文件是否存在
        /// <summary>
        ///  检测指定文件是否存在,如果存在则返回true
        /// </summary>
        /// <param name="path">文件的绝对路径</param>
        /// <returns></returns>
        public static bool IsExistFile(string path)
        {
            return File.Exists(path);
        }
        #endregion

        #region CreateFolder 创建文件夹
        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="path">路径</param>
        public static void CreateFolder(string path)
        {
            if (string.IsNullOrEmpty(path))
                return;
            if (!IsExistFolder(path))
                Directory.CreateDirectory(path);
        }
        #endregion

        #region CreateFile 创建文件
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="path"></param>
        public static void CreateFile(string path)
        {
            //如果文件不存在则创建该文件
            if (!IsExistFile(path))
            {
                //创建一个FileInfo对象
                FileInfo file = new FileInfo(path);

                //创建文件
                FileStream fs = file.Create();

                //关闭文件流
                fs.Close();
            }
        }
        #endregion

        #region Steam2File 流写入文件(流、字节写入)
        /// <summary>
        /// 流写入并创建文件 (流、字节写入)
        /// </summary>
        /// <param name="path"></param>
        /// <param name="stream"></param>
        public static string Steam2File(string path, Stream stream)
        {
            byte[] bt = new byte[1024];
            int i = 0;
            FileStream file = null;
            try
            {
                file = new FileStream(path, FileMode.Create);

                while ((i = stream.Read(bt, 0, bt.Length)) != 0)
                {
                    file.Write(bt, 0, i);
                    file.Flush();//清除此流的缓冲区，使得所有缓冲的数据都写入到文件中，若不调用此方法，则在关闭下次write时或关闭流时写入文件
                }
                return path;
            }
            catch (Exception ex)
            {
                return path + "\n" + ex.Message;
            }
            finally
            {
                file.Close();
                stream.Close();
            }
        }
        #endregion

        #region File2Stream 文件写入流
        /// <summary>
        /// 文件写入流
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Stream File2Stream(string path)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

            byte[] bt = new byte[file.Length];
            file.Read(bt, 0, bt.Length);
            file.Close();

            //把byte[] 转 Stream
            Stream stream = new MemoryStream(bt);
            return stream;
        }
        #endregion

        #region File2String 读取文件
        /// <summary>
        ///File2String 读取文件（使用StreamReader）
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static String File2String(string path)
        {
            string temp = string.Empty;
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            StreamReader sr = new StreamReader(file);
            while (sr.Peek() != -1)
            {
                temp += sr.ReadLine() + "\n";
            }
            return temp;
        }
        #endregion

        #region  String2File 写入或追加
        /// <summary>
        /// 文件写入（追加）UTF-8编码文本
        /// </summary>
        /// <param name="path">文件路径</param>
        /// <param name="content">文件内容</param>
        /// <param name="endFlag">结束添加标记（换行‘\n’）</param>
        public static bool String2File(String path, String content, String endFlag)
        {
            try
            {
                StreamWriter writer;
                if (!IsExistFile(path))
                    writer = File.CreateText(path);
                else
                    writer = File.AppendText(path);
                writer.Write(content);
                writer.Write(endFlag);
                writer.Flush();
                writer.Close();
                writer.Dispose();
                return true;
            }
            catch (Exception)
            {

            }
            return false;
        }
        #endregion

        #region GetDirectories 获取指定目录及子目录
        /// <summary>
        /// GetDirectories 获取指定目录及子目录
        /// </summary>
        /// <param name="path">指定目录的绝对路径</param>
        /// <param name="searchPattern">模式字符串，"*"代表0或N个字符，"?"代表1个字符</param>
        /// <param name="isSearchChild">是否搜索子目录</param>
        /// <returns></returns>
        public static string[] GetDirectories(string path, string searchPattern, bool isSearchChild)
        {
            if (isSearchChild)
                return Directory.GetDirectories(path, searchPattern, SearchOption.AllDirectories);
            else
                return Directory.GetDirectories(path, searchPattern, SearchOption.TopDirectoryOnly);
        }
        /// <summary>
        ///  GetDirectories 获取指定目录及子目录
        /// </summary>
        /// <param name="path"></param>
        /// <param name="isSearchChild"></param>
        /// <returns></returns>
        public static string[] GetDirectories(string path, bool isSearchChild)
        {
            if (isSearchChild)
                return Directory.GetDirectories(path, "*", SearchOption.AllDirectories);
            else
                return Directory.GetDirectories(path, "*", SearchOption.TopDirectoryOnly);
        }
        #endregion

        #region GetFiles 获取指定目录的文件及子目录文件
        /// <summary>
        /// GetFiles
        /// </summary>
        /// <param name="path"></param>
        /// <param name="searchPattern"></param>
        /// <param name="isSearchChild"></param>
        /// <returns></returns>
        public static string[] GetFiles(string path, string searchPattern, bool isSearchChild)
        {
            if (isSearchChild)
                return Directory.GetFiles(path, searchPattern, SearchOption.AllDirectories);
            else
                return Directory.GetFiles(path, searchPattern, SearchOption.TopDirectoryOnly);
        }
        /// <summary>
        /// GetFiles
        /// </summary>
        /// <param name="path"></param>
        /// <param name="isSearchChild"></param>
        /// <returns></returns>
        public static string[] GetFiles(string path, bool isSearchChild)
        {
            if (isSearchChild)
                return Directory.GetFiles(path, "*", SearchOption.AllDirectories);
            else
                return Directory.GetFiles(path, "*", SearchOption.TopDirectoryOnly);
        }
        #endregion

        #region  MoveFile CopyFile DeleteFile DeleteDirectory 移动 删除 复制 文件
        /// <summary>
        /// 移动文件（不会创建目录）
        /// </summary>
        /// <param name="oldPath">原路径</param>
        /// <param name="newPath">新路径</param>
        /// <returns></returns>
        public static bool MoveFile(string oldPath, string newPath)
        {
            try
            {
                if (IsExistFile(oldPath))
                {
                    File.Move(oldPath, newPath);
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
        /// <summary>
        /// 移动文件夹
        /// </summary>
        /// <param name="oldPath">旧路径</param>
        /// <param name="newPath">新路径</param>
        /// <returns></returns>
        public static bool MoveDirectory(string oldPath, string newPath)
        {
            try
            {
                if (IsExistFolder(oldPath))
                {
                    Directory.Move(oldPath, newPath);
                    return true;
                }
            }
            catch (Exception)
            {
            }
            return false;
        }
        /// <summary>
        /// 复制文件（不会创建目录）
        /// </summary>
        /// <param name="oldPath"></param>
        /// <param name="newPath"></param>
        /// <returns></returns>
        public static bool CopyFile(string oldPath, string newPath)
        {
            try
            {
                if (IsExistFile(oldPath))
                {
                    File.Copy(oldPath, newPath);
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool DeleteFile(string path)
        {
            try
            {
                if (IsExistFile(path))
                {
                    File.Delete(path);
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
        /// <summary>
        /// 删除文件夹（子文件）
        /// </summary>
        /// <param name="path">路径</param>
        /// <param name="isChildDel">是否删除子目录</param>
        /// <returns></returns>
        public static bool DeleteDirectory(string path, bool isChildDel)
        {
            try
            {
                if (IsExistFolder(path))
                {
                    Directory.Delete(path, isChildDel);
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
        #endregion
    }
}
