﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Urs.Framework.Infrastructure.TagHelpers
{
    /// <summary>
    /// u-label tag helper
    /// </summary>
    [HtmlTargetElement("u-bool", Attributes = ForAttributeName, TagStructure = TagStructure.WithoutEndTag)]
    public class UBoolTagHelper : TagHelper
    {
        private const string ForAttributeName = "asp-for";

        /// <summary>
        /// HtmlGenerator
        /// </summary>
        protected IHtmlGenerator Generator { get; set; }

        [HtmlAttributeName(ForAttributeName)]
        public ModelExpression For { get; set; }

        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public UBoolTagHelper(IHtmlGenerator generator)
        {
            Generator = generator;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            //clear the output
            output?.SuppressOutput();
            var tagBuilder = Generator.GenerateCheckBox(ViewContext, For.ModelExplorer, For.Name, null,null);
            tagBuilder.Attributes.Add("lay-skin", "switch");
            tagBuilder.Attributes.Add("lay-text", "是|否");
            tagBuilder.Attributes.Add("lay-filter", For.Name);

            output?.Content.SetHtmlContent(tagBuilder);
        }
    }
}