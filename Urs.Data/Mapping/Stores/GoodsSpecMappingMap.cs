
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsSpecMappingMap : UrsEntityTypeConfiguration<GoodsSpecMapping>
    {
        public override void Configure(EntityTypeBuilder<GoodsSpecMapping> builder)
        {
            builder.ToTable(nameof(GoodsSpecMapping));
            builder.HasKey(pva => pva.Id);

            base.Configure(builder);
        }
    }
}