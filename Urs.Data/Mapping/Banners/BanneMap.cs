using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Mapping;
using Urs.Data.Domain.Banners;

namespace Urs.Data.Mapping.Banners
{
    public partial class BanneMap : UrsEntityTypeConfiguration<Banner>
    {
        public override void Configure(EntityTypeBuilder<Banner> builder)
        {
            builder.ToTable(nameof(Banner));
            builder.HasKey(x => x.Id);

            base.Configure(builder);
        }
    }
}