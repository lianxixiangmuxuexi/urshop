namespace Urs.Data.Domain.Users
{
    public enum UserRegistrationType : int
    {
        /// <summary>
        /// 管理员审核
        /// </summary>
        AdminApproval = 3,
        /// <summary>
        /// 不允许注册
        /// </summary>
        Disabled = 4,
        /// <summary>
        /// 手机验证
        /// </summary>
        Phone = 5,
        /// <summary>
        /// 自动授权
        /// </summary>
        AutoToken = 6
    }
}
