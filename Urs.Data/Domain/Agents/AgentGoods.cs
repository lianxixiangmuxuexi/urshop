
using Urs.Core;

namespace Urs.Data.Domain.Agents
{
    public partial class AgentGoods : BaseEntity
    {
        public virtual int ProductId { get; set; }
        /// <summary>
        /// 分销商、团长、卖家 佣金
        /// </summary>
        public virtual decimal Rate { get; set; }
        /// <summary>
        /// 经销商、批发商 代理商佣金
        /// </summary>
        public virtual decimal ParentRate { get; set; }

    }
}
