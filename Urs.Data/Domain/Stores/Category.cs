using Urs.Core;
using System;

namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// Represents a category
    /// </summary>
    public partial class Category : BaseEntity
    {
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public virtual string Name { get; set; }
        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public virtual string Description { get; set; }
        /// <summary>
        /// Gets or sets the parent category identifier
        /// </summary>
        public virtual int ParentCategoryId { get; set; }
        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public virtual int PictureId { get; set; }
        /// <summary>
        /// Gets or sets the page size
        /// </summary>
        public virtual int PageSize { get; set; }
        /// <summary>
        /// Gets or sets the available price ranges
        /// </summary>
        public virtual string PriceRanges { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether to show the category on home page
        /// </summary>
        public virtual bool ShowOnHomePage { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public virtual bool Published { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public virtual bool Deleted { get; set; }
        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public virtual int DisplayOrder { get; set; }
        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public virtual DateTime CreateTime { get; set; }
        /// <summary>
        /// Gets or sets the date and time of instance update
        /// </summary>
        public virtual DateTime UpdateTime { get; set; }

    }
}
