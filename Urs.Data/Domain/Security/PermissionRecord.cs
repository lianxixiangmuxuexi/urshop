﻿using Urs.Core;
using System.Collections.Generic;

namespace Urs.Data.Domain.Security
{
    /// <summary>
    /// Represents a permission record
    /// </summary>
    public class PermissionRecord : BaseEntity
    {
        private ICollection<PermissionRecordUserMapping> _userRoles;

        /// <summary>
        /// Gets or sets the permission name
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the permission system name
        /// </summary>
        public virtual string SystemName { get; set; }
        
        /// <summary>
        /// Gets or sets the permission category
        /// </summary>
        public virtual string Group { get; set; }
        
        /// <summary>
        /// Gets or sets discount usage history
        /// </summary>
        public virtual ICollection<PermissionRecordUserMapping> UserRoles
        {
            get { return _userRoles ?? (_userRoles = new List<PermissionRecordUserMapping>()); }
            protected set { _userRoles = value; }
        }   
    }
}
