﻿
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class AdminAreaSettings : ISettings
    {
        public int GridPageSize { get; set; }
        public string GridPageSizeArray { get; set; }
        public bool DisplayGoodsPictures { get; set; }
    }
}