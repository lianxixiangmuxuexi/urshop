﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using Urs.Core;

namespace Urs.Services.ExportImport.Help
{
    public class PropertyByName<T>
    {
        private object _propertyValue;

        public PropertyByName(string propertyName, Func<T, object> func = null, bool ignore = false)
        {
            this.PropertyName = propertyName;
            this.GetProperty = func;
            this.PropertyOrderPosition = 1;
            this.Ignore = ignore;
        }

        public int PropertyOrderPosition { get; set; }

        public Func<T, object> GetProperty { get; private set; }

        public string PropertyName { get; private set; }

        public object PropertyValue
        {
            get
            {
                return IsDropDownCell ? GetItemId(_propertyValue) : _propertyValue;
            }
            set
            {
                _propertyValue = value;
            }
        }

        public int IntValue
        {
            get
            {
                int rez;
                if (PropertyValue == null || !int.TryParse(PropertyValue.ToString(), out rez))
                    return default(int);
                return rez;
            }
        }

        public bool BooleanValue
        {
            get
            {
                bool rez;
                if (PropertyValue == null || !bool.TryParse(PropertyValue.ToString(), out rez))
                    return default(bool);
                return rez;
            }
        }

        public string StringValue
        {
            get
            {
                return PropertyValue == null ? string.Empty : Convert.ToString(PropertyValue);
            }
        }

        public decimal DecimalValue
        {
            get
            {
                decimal rez;
                if (PropertyValue == null || !decimal.TryParse(PropertyValue.ToString(), out rez))
                    return default(decimal);
                return rez;
            }
        }

        public decimal? DecimalValueNullable
        {
            get
            {
                decimal rez;
                if (PropertyValue == null || !decimal.TryParse(PropertyValue.ToString(), out rez))
                    return null;
                return rez;
            }
        }

        public double DoubleValue
        {
            get
            {
                double rez;
                if (PropertyValue == null || !double.TryParse(PropertyValue.ToString(), out rez))
                    return default(double);
                return rez;
            }
        }

        public DateTime? DateTimeNullable
        {
            get
            {
                return PropertyValue == null ? null : DateTime.FromOADate(DoubleValue) as DateTime?;
            }
        }

        public override string ToString()
        {
            return PropertyName;
        }

        public bool Ignore { get; set; }

        public bool IsDropDownCell
        {
            get { return DropDownElements != null; }
        }

        public string[] GetDropDownElements()
        {
            return IsDropDownCell ? DropDownElements.Select(ev => ev.Text).ToArray() : new string[0];
        }

        public string GetItemText(object id)
        {
            return DropDownElements.FirstOrDefault(ev => ev.Value == id.ToString())?.Text ?? String.Empty;
        }

        public int GetItemId(object name)
        {
            if (string.IsNullOrEmpty(name?.ToString()))
                return 0;

            if (!int.TryParse(name.ToString(), out var id))
            {
                id = 0;
            }

            return Convert.ToInt32(DropDownElements.FirstOrDefault(ev => ev.Text.Trim() == name.ToString().Trim())?.Value ?? id.ToString());
        }

        public SelectList DropDownElements { get; set; }

        public bool AllowBlank { get; set; }

        public bool IsCaption
        {
            get { return PropertyName == StringValue || PropertyName == _propertyValue.ToString(); }
        }
    }
}
