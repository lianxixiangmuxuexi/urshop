﻿using Urs.Core.Caching;
using Urs.Services.Tasks;

namespace Urs.Services.Caching
{
    public partial class ClearCacheTask : IScheduleTask
    {
        #region Fields

        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public ClearCacheTask(ICacheManager cacheManager)
        {
            this._cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public void Execute()
        {
            _cacheManager.Clear();
        }

        #endregion
    }
}