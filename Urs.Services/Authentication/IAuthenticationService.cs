using Urs.Data.Domain.Users;

namespace Urs.Services.Authentication
{
    public partial interface IAuthenticationService
    {
        void SignIn(User user, bool createPersistentCookie);
        string JwtSignIn(User user, string securityKey, int expiresdays, string issuer, string audience);
        void SignOut();
        User GetAuthenticatedUser();
    }
}