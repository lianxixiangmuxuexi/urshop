using System.Collections.Generic;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Shipping;
using Urs.Services.Payments;

namespace Urs.Services.Orders
{
    public partial interface IOrderProcessingService
    {
        string GetSku(Goods goods, string xml);
        PlaceOrderResult PlaceOrder(ProcessPaymentRequest processPaymentRequest);

        PlaceOrderResult QuickPlaceOrder(ProcessPaymentRequest processPaymentRequest);
        void DeleteOrder(Order order);

        void Ship(Shipment shipment, bool notifyUser);

        void Deliver(Shipment shipment, bool notifyUser);
        void ConfirmDeliver();
        void Complete(int orderId, bool notifyUser);
        bool CanCancelOrder(Order order);

        void CancelOrder(Order order, bool notifyUser);
        void CancelUnpaidOrder();
        bool CanMarkOrderAsAuthorized(Order order);

        void MarkAsAuthorized(Order order);



        bool CanCapture(Order order);

        IList<string> Capture(Order order);

        bool CanMarkOrderAsPaid(Order order);

        void MarkOrderAsPaid(Order order);



        bool CanRefund(Order order);

        IList<string> Refund(Order order);

        bool CanRefundOffline(Order order);

        void RefundOffline(Order order);

        bool CanPartiallyRefund(Order order, decimal amountToRefund);

        IList<string> PartiallyRefund(Order order, decimal amountToRefund);

        bool CanPartiallyRefundOffline(Order order, decimal amountToRefund);

        void PartiallyRefundOffline(Order order, decimal amountToRefund);



        bool CanVoid(Order order);

        IList<string> Void(Order order);

        bool CanVoidOffline(Order order);

        void VoidOffline(Order order);




        void ReOrder(Order order);

        bool IsAfterSalesAllowed(Order order);

        bool IsCancelAllowed(Order order);

        bool IsDeleteAllowed(Order order);

        bool IsReviewAllowed(Order order);

        bool IsPendingAllowed(Order order);
        bool ValidateMinOrderSubtotalAmount(IList<ShoppingCartItem> cart);

        bool ValidateMinOrderTotalAmount(IList<ShoppingCartItem> cart, User user = null);
    }
}
