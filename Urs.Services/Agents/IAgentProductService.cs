
using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Agents;

namespace Urs.Services.Agents
{
    public partial interface IAgentProductService
    {
        void Delete(AgentGoods entity);

        AgentGoods GetById(int id);

        IPagedList<AgentGoods> GetAll(int pageIndex, int pageSize);

        IList<AgentGoods> GetListByProductIds(int[] ids);

        void Insert(AgentGoods entity);

        void Update(AgentGoods entity);
    }
}
