using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Users;

namespace Urs.Services.Stores
{
    public partial interface IGoodsSpecFormatter
    {
        string FormatAttributes(string attributes, string serapator = "<br />", bool htmlEncode = true, bool renderPrices = true,
            bool renderGoodsSpecs = true, bool renderGiftCardAttributes = true,
            bool allowHyperlinks = true);
    }
}
