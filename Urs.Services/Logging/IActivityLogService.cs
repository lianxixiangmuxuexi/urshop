﻿using System;
using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Logging;

namespace Urs.Services.Logging
{
    public partial interface IActivityLogService
    {
        void InsertActivityType(ActivityLogType activityLogType);

        void UpdateActivityType(ActivityLogType activityLogType);
                
        void DeleteActivityType(ActivityLogType activityLogType);
        
        IList<ActivityLogType> GetAllActivityTypes();
        
        ActivityLogType GetActivityTypeById(int activityLogTypeId);
        
        ActivityLog InsertActivity(string systemKeyword, string comment, params object[] commentParams);

        ActivityLog InsertActivity(string systemKeyword, 
            string comment, User user, params object[] commentParams);

        void DeleteActivity(ActivityLog activityLog);

        IPagedList<ActivityLog> GetAllActivities(DateTime? createdOnFrom,
            DateTime? createdOnTo, int? userId,
            int activityLogTypeId, int pageIndex, int pageSize);
        
        ActivityLog GetActivityById(int activityLogId);

        void ClearAllActivities();
    }
}
