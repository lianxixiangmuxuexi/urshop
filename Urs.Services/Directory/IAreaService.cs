using System.Collections.Generic;
using Urs.Data.Domain.Directory;

namespace Urs.Services.Directory
{
    public partial interface IAreaService
    {
        void DeleteArea(Area Area);
        IList<string> GetProvinces();
        IList<string> GetCities(string provincename);
        IList<string> GetAreas(string provincename, string cityname);

        IList<AreaCodeName> GetProvincesByCode();
        IList<AreaCodeName> GetCitiesByProvinceCode(string provinceCode);
        IList<AreaCodeName> GetAreasByCode(string provinceCode, string cityCode);

        AreaCodeName GetProvinceByName(string provinceName);
        AreaCodeName GetCityByName(string provinceName, string cityName);
        AreaCodeName GetAreaByName(string provinceName, string cityName, string areaName);

        AreaCodeName GetProvinceByCode(string provinceCode);
        AreaCodeName GetCityByCode(string provinceCode, string cityCode);
        AreaCodeName GetAreaByCode(string provinceCode, string cityCode, string areaCode);

        Area GetCityByCityCode(string cityCode);
        void InsertArea(Area Area);

        void UpdateArea(Area Area);
    }
}
