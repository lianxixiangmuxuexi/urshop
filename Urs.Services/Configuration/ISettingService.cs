using System.Collections.Generic;
using Urs.Core.Configuration;
using Urs.Data.Domain.Configuration;

namespace Urs.Services.Configuration
{
    public partial interface ISettingService
    {
        Setting GetSettingById(int settingId);

        Setting GetSettingByKey(string key);

        T GetSettingByKey<T>(string key, T defaultValue = default(T));
        
        void SetSetting<T>(string key, T value, bool clearCache = true);

        void DeleteSetting(Setting setting);

        IList<Setting> GetAllSettings();


        T LoadSetting<T>() where T : ISettings, new();
        void SaveSetting<T>(T settingInstance) where T : ISettings, new();

        void DeleteSetting<T>() where T : ISettings, new();

        void ClearCache();
    }
}
