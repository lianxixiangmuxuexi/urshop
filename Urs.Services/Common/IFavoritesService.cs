using Urs.Core;
using Urs.Data.Domain.Common;

namespace Urs.Services.Common
{
    public partial interface IFavoritesService
    {
        #region form favorites
        void DeleteFavorites(Favorites favorites);

        Favorites GetById(int favoritesId);
        IPagedList<Favorites> GetFavorites(int userId, int pageIndex, int pageSize);
        void InsertFavorites(Favorites favorites);

        void UpdateFavorites(Favorites favorites);

        #endregion

    }
}